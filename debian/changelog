python-musicbrainz2 (0.7.4-3) UNRELEASED; urgency=medium

  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient XS-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <novy@ondrej.org>  Tue, 29 Mar 2016 22:11:49 +0200

python-musicbrainz2 (0.7.4-2) unstable; urgency=medium

  * Team upload.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  * Run tests only if DEB_BUILD_OPTIONS=nocheck is not set.

  [ Piotr Ożarowski ]
  * Convert from dh_pysupport to dh_python2 (Closes: #786005)

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 19 Aug 2015 23:27:55 +0200

python-musicbrainz2 (0.7.4-1) unstable; urgency=low

  * New upstream release
  * debian/rules
    - guard tests loop with set -e

 -- Sandro Tosi <morph@debian.org>  Sat, 19 Nov 2011 23:03:09 +0100

python-musicbrainz2 (0.7.3-1) unstable; urgency=low

  * New upstream release; Closes: #588932, #587225
  * Use debhelper 7
  * debian/control
    - new maintainer; Closes: #628473
    - we now request python >= 2.6, drop alternative depends for ctypes
    - add misc:Depends to binary packages
    - removed article at the beginning of short description
    - removed Provides, not needed
    - bump Standards-Version to 3.9.2 (no changes needed)
    - updated Vcs-{Browser, Svn} enties
  * debian/copyright
    - added debian packaging copyright & license notice
  * debian/source/format
    - use 3.0 (quilt) source format
  * debian/{control, pyversions}
    - remove pyversions, converted into XS-Python-Version
  * debian/pycompat
    - removed, unused
  * debian/{control, rules}
    - run tests for all supported python versions
  * debian/python-musicbrainz2-doc.doc-base
    - added doc-base registation file

 -- Sandro Tosi <morph@debian.org>  Fri, 10 Jun 2011 16:14:29 +0200

python-musicbrainz2 (0.6.0-2) unstable; urgency=low

  * Remove dependency on python-ctypes if python >= 2.5 is installed.
    Patch by Fabrice Silva. (Closes: #503872)
  * Change policy version to 3.8.0 (no changes).

 -- Lukáš Lalinský <lalinsky@gmail.com>  Sun, 02 Nov 2008 15:26:15 +0100

python-musicbrainz2 (0.6.0-1) unstable; urgency=low

  * New upstream release.
  * Change policy version to 3.7.3.
  * Replace homepage in the description with a Homepage field.
  * Add Vcs-Bzr field.

 -- Lukáš Lalinský <lalinsky@gmail.com>  Fri, 23 May 2008 15:41:59 +0200

python-musicbrainz2 (0.4.1-1) unstable; urgency=low

  * New upstream release.
  * Added Depends on libdiscid0.
  * Moved to python-musicbrainz2-doc to section doc.

 -- Lukáš Lalinský <lalinsky@gmail.com>  Tue, 19 Dec 2006 11:02:52 +0100

python-musicbrainz2 (0.3.1-1) unstable; urgency=low

  * Initial release. (Closes: #370255)

 -- Lukáš Lalinský <lalinsky@gmail.com>  Sun, 26 Aug 2006 08:02:00 +0200
